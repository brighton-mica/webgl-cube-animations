var vertexShaderSource = `#version 300 es
in vec4 a_pos;

uniform vec4 u_color;
uniform mat4 u_model;
uniform mat4 u_projection;
uniform mat4 u_view;

void main() {
    gl_Position = u_projection * u_view * u_model * a_pos;
}
`

var fragmentShaderSource = `#version 300 es
precision highp float;

uniform vec4 u_color;

out vec4 outColor;

void main() {
    outColor = u_color;
}
`

//    // Slider Setup
//    let camera = {
//     position : [0, 0, 0],
//     rotation : [0, 0, 0],
// }

// let cameraXPos = document.getElementById("camera-x-trans-slider")
// let cameraYPos = document.getElementById("camera-y-trans-slider")
// let cameraZPos = document.getElementById("camera-z-trans-slider")
// let cameraXRot = document.getElementById("camera-x-rot-slider")
// let cameraYRot = document.getElementById("camera-y-rot-slider")
// let cameraZRot = document.getElementById("camera-z-rot-slider")


// cameraXPos.min = -gl.canvas.width
// cameraXPos.max = gl.canvas.width
// cameraXPos.value = 0
// cameraYPos.min = -gl.canvas.height
// cameraYPos.max = gl.canvas.height
// cameraYPos.value = 0
// cameraZPos.min = 0
// cameraZPos.max = 500
// cameraZPos.value = 0
// cameraXRot.min = 0
// cameraXRot.max = 360
// cameraXRot.value = 0
// cameraYRot.min = 0
// cameraYRot.max = 360
// cameraYRot.value = 0
// cameraZRot.min = 0
// cameraZRot.max = 360
// cameraZRot.value = 0

// cameraXPos.oninput = e => UpdateCameraPos(0, event.target.value)
// cameraYPos.oninput = e => UpdateCameraPos(1, event.target.value)
// cameraZPos.oninput = e => UpdateCameraPos(2, event.target.value)
// cameraXRot.oninput = e => UpdateCameraRot(0, event.target.value)
// cameraYRot.oninput = e => UpdateCameraRot(1, event.target.value)
// cameraZRot.oninput = e => UpdateCameraRot(2, event.target.value)

// function UpdateCameraPos(index, value) {
//     camera.position[index] = value
//     DrawScene()
// }

// function UpdateCameraRot(index, value) {
//     camera.rotation[index] = value * Math.PI / 180
//     DrawScene()
// }

function main() {

    // WebGL
    const canvas = document.getElementById("gl-canvas")
    const gl = canvas.getContext("webgl2")
    if (!gl) {
        console.error("WebGL not supported!")
        return
    }

    let period = gl.canvas.width / Math.PI
    let displayIndex = 0
    let display = document.getElementById("func")
    display.onchange = e => UpdateDisplay(event.target.value)

    let displays = [
        (model, cube, now) => mat4.scale(model, 1, 1 + 4 * Math.abs(Math.sin((1/period) * cube[0] + now)) * 3 * Math.abs(Math.cos((1/period) * cube[2] + now)), 1),
        (model, cube, now) => mat4.scale(model, 1, 1 + 7 * Math.sin((1 / period) * cube[0] + cube[2] + now), 1),
        (model, cube, now) => mat4.scale(model, 1, 1 + 5 * Math.sin((1 / period) * cube[0] * cube[2] + now), 1),
        (model, cube, now) => mat4.scale(model, 1, 1 + 5 * Math.abs(Math.sin((period/2) * (cube[0]*cube[0] + cube[2]*cube[2]) + now)), 1)

    ]

    function UpdateDisplay(val) {
        displayIndex = val - 1
    }

 

    // Vertex Shader
    const vertexShader = gl.createShader(gl.VERTEX_SHADER)
    gl.shaderSource(vertexShader, vertexShaderSource)
    gl.compileShader(vertexShader)
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        console.error("Error compiling vertex shader")
        console.log(gl.getShaderInfoLog(vertexShader));
        gl.deleteShader(vertexShader);
        return
    }

    // Fragment Shader
    const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER)
    gl.shaderSource(fragmentShader, fragmentShaderSource)
    gl.compileShader(fragmentShader)
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        console.error("Error compiling fragment shader")
        console.log(gl.getShaderInfoLog(fragmentShader));
        gl.deleteShader(fragmentShader);
        return
    }

    // Shader Program
    const shaderProgram = gl.createProgram()
    gl.attachShader(shaderProgram, vertexShader)
    gl.attachShader(shaderProgram, fragmentShader)
    gl.linkProgram(shaderProgram)
    if(!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        console.error("Error linking shader program")
        return
    }

    let cubeSize = 10
    let cubeVertices = [

        // front
       -cubeSize, -cubeSize,  cubeSize, 
        cubeSize, -cubeSize,  cubeSize,
        cubeSize,  cubeSize,  cubeSize,
       -cubeSize, -cubeSize,  cubeSize,
        cubeSize,  cubeSize,  cubeSize,
       -cubeSize,  cubeSize,  cubeSize,

       // back
       -cubeSize, -cubeSize, -cubeSize,
        cubeSize,  cubeSize, -cubeSize,
        cubeSize, -cubeSize, -cubeSize,
       -cubeSize, -cubeSize, -cubeSize,
       -cubeSize,  cubeSize, -cubeSize,
        cubeSize,  cubeSize, -cubeSize,

        // left
       -cubeSize, -cubeSize, -cubeSize,
       -cubeSize, -cubeSize,  cubeSize,
       -cubeSize,  cubeSize,  cubeSize,
       -cubeSize, -cubeSize, -cubeSize,
       -cubeSize,  cubeSize,  cubeSize,
       -cubeSize,  cubeSize, -cubeSize,

        // right
        cubeSize, -cubeSize,  cubeSize,
        cubeSize, -cubeSize, -cubeSize,
        cubeSize,  cubeSize, -cubeSize,
        cubeSize, -cubeSize,  cubeSize,
        cubeSize,  cubeSize, -cubeSize,
        cubeSize,  cubeSize,  cubeSize,

        // top
       -cubeSize,  cubeSize,  cubeSize,
        cubeSize,  cubeSize,  cubeSize,
        cubeSize,  cubeSize, -cubeSize,
       -cubeSize,  cubeSize,  cubeSize,
        cubeSize,  cubeSize, -cubeSize,
       -cubeSize,  cubeSize, -cubeSize,

        // bottom
       -cubeSize, -cubeSize,  cubeSize,
        cubeSize, -cubeSize, -cubeSize,
        cubeSize, -cubeSize,  cubeSize,
       -cubeSize, -cubeSize,  cubeSize,
       -cubeSize, -cubeSize, -cubeSize,
        cubeSize, -cubeSize, -cubeSize,
    ]

    // Cube Buffer
    let cubeBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeBuffer)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertices), gl.STATIC_DRAW)

    // Attrib and Uniform Locations
    let a_pos = gl.getAttribLocation(shaderProgram, "a_pos")
    let u_color = gl.getUniformLocation(shaderProgram, "u_color")
    let u_model = gl.getUniformLocation(shaderProgram, "u_model")
    let u_projection = gl.getUniformLocation(shaderProgram, "u_projection")
    let u_view = gl.getUniformLocation(shaderProgram, "u_view")

    // Cube VAO
    let cubeVAO = gl.createVertexArray()
    gl.bindVertexArray(cubeVAO)

        gl.enableVertexAttribArray(a_pos)
        gl.vertexAttribPointer(a_pos, 3, gl.FLOAT, false, 0, 0)

    gl.bindVertexArray(null)


    // Drawing Setup
    let projection = mat4.ortho(0, 500, 0, 500, -50, 550)
    let view = mat4.identity()
    view = mat4.rotateX(view, 20 * Math.PI / 180)
    view = mat4.rotateY(view, 353 * Math.PI / 180)
    view = mat4.rotateZ(view, 0)
    view = mat4.translate(view, -30, -76, 0)
    view = mat4.inverse(view)
    // let projection = mat4.perspective((1/3) * Math.PI, 50, -50000, gl.canvas.clientWidth / gl.canvas.clientHeight)

    cubes = []
    let incr = cubeSize * 2
    for (let x = 50; x <= gl.canvas.width - 50; x += incr) {
        for (let z = 50; z < gl.canvas.width - 50; z += incr) {
            cubes.push([x, 100, z])
        }
    }

    requestAnimationFrame(DrawScene)

    function DrawScene(now) {
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height)
        gl.clearColor(0.8, 0.9, 0.8, 1.0)
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
        // gl.enable(gl.CULL_FACE)
        gl.enable(gl.DEPTH_TEST)

        now *= 0.0005

        gl.bindVertexArray(cubeVAO)
        {
            gl.useProgram(shaderProgram)
            {
                gl.uniformMatrix4fv(u_view, false, view)
                gl.uniformMatrix4fv(u_projection, false, projection)

                cubes.forEach(cube => {
                    let period = gl.canvas.width / Math.PI
                    let model = mat4.identity()
                    model = displays[displayIndex](model, cube, now)
                    // model = mat4.scale(model, 1, 1 + 3 * Math.abs(Math.sin((1/period) * cube[0] + now)) * 3 * Math.abs(Math.cos((1/period) * cube[2] + now)), 1)
                    // model = mat4.scale(model, 1, 1 + 1 * Math.sin((1 / period) * cube[0] + cube[2] + now), 1)
                    // model = mat4.scale(model, 1, 1 + 5 * Math.sin((1 / period) * cube[0] * cube[2] + now), 1)

                    //model = mat4.scale(model, 1, 1 + 5 * Math.abs(Math.sin((period/2) * (cube[0]*cube[0] + cube[2]*cube[2]) + now)), 1)
                    model = mat4.translate(model, cube[0], cube[1], -cube[2])
    
                    gl.uniformMatrix4fv(u_model, false, model)

                    gl.uniform4f(u_color, 0.1, 0.1, 0.1, 1.0) // front
                    gl.drawArrays(gl.TRIANGLES, 0, 6)
                    gl.uniform4f(u_color, 0.7, 0.7, 0.7, 1.0)
                    gl.drawArrays(gl.TRIANGLES, 6, 6)
                    gl.uniform4f(u_color, 0.0, 0.3, 0.3, 1.0) // left
                    gl.drawArrays(gl.TRIANGLES, 12, 6)
                    gl.uniform4f(u_color, 1.0, 1.0, 1.0, 1.0)
                    gl.drawArrays(gl.TRIANGLES, 18, 6)
                    gl.uniform4f(u_color, 0.4, 0.8, 0.9, 1.0) // top
                    gl.drawArrays(gl.TRIANGLES, 24, 6)
                    gl.uniform4f(u_color, 0.4, 0.8, 0.9, 1.0)
                    gl.drawArrays(gl.TRIANGLES, 30, 6)

                })
            }
        }
        gl.useProgram(null)
        gl.bindVertexArray(null)
        requestAnimationFrame(DrawScene)
    }
}

main();